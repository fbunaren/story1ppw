from django.http import HttpResponse            # pragma: no cover
from django.shortcuts import render, redirect   # pragma: no cover
from django.urls import reverse

def home(request):
    return render(request,'index.html')